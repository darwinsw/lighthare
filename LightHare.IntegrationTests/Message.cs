﻿namespace LightHare.IntegrationTests {
    public class ParentMessage {
        public int IntValue { get; set; }

        protected bool Equals(ParentMessage other) {
            return IntValue == other.IntValue;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ParentMessage) obj);
        }

        public override int GetHashCode() {
            return IntValue;
        }
    }

    public class ChildMessage : ParentMessage {
        public string StringValue { get; set; }

        protected bool Equals(ChildMessage other) {
            return IntValue == other.IntValue && string.Equals(StringValue, other.StringValue);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ChildMessage) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (IntValue * 397) ^ (StringValue != null ? StringValue.GetHashCode() : 0);
            }
        }
    }
}