﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using RabbitMQ.Client;

namespace LightHare.IntegrationTests {
    [TestFixture]
    public class PublishAndSubscribeTest {
        private static string GetRabbitMqUri() {
            return Environment.GetEnvironmentVariable("INTEGRATION_RABBITMQ_URI") ??
                   "amqp://guest:guest@localhost/integration-tests";
        }

        private static readonly string ExchangeName = typeof(ChildMessage).AssemblyQualifiedName;
        private static readonly TimeSpan Timeout = TimeSpan.FromMilliseconds(250);
        private const string CustomExchangeName = "TheExchange";
        private const string QueueName = "TheQueue"; 
        private const string NoRoutingKey = "";
        private const bool Durable = true;
        private const bool NotExclusive = false;
        
        private MessageBroker broker;
        private IConnection connection;
        private IModel channel;

        [SetUp]
        public Task InitAndStartBrokerAndFixtures() {
            connection = new ConnectionFactory { Uri = new Uri(GetRabbitMqUri()), AutomaticRecoveryEnabled = true, TopologyRecoveryEnabled = true}
                .CreateConnection();
            channel = connection.CreateModel();
            
            channel.ExchangeDeclare(ExchangeName, ExchangeType.Fanout, true);
            channel.ExchangeDeclare(CustomExchangeName, ExchangeType.Fanout, true);
            channel.QueueDeclare(QueueName, Durable, NotExclusive);
            channel.QueueBind(QueueName, ExchangeName, NoRoutingKey);
            channel.QueueBind(QueueName, CustomExchangeName, NoRoutingKey);
            
            broker = new MessageBroker(GetRabbitMqUri(), QueueName);
            return broker.Start();
        }
        
        [TearDown]
        public Task StopBroker() {
            channel.Close();
            connection.Close();
            return broker.Stop();
        }

        [Test]
        public void CanReadRabbitMqUri() {
            Assert.That(GetRabbitMqUri(),
                Is.EqualTo("amqp://guest:MyNotSoStrongPassw0rd321!@rabbitmqtest/integration-tests")
                    .Or.EqualTo("amqp://guest:guest@localhost/integration-tests")
            );
        }
        
        [Test]
        public async Task SubscribeAndPublishOnImplicitExchange() {
            SimpleSubscriber<ChildMessage> subscriber = new SimpleSubscriber<ChildMessage>(1234);
            broker.Subscribe<ChildMessage>(subscriber);
            ChildMessage msg = new ChildMessage() { IntValue = 19, StringValue = "Pietro" };
            await broker.Publish(msg);
            await Task.Delay(250);
            Assert.That(subscriber.ReceivedMsg, Is.EqualTo(msg));
        }
        
        [Test]
        public async Task MessageTypeIsAvailableInSubscriber() {
            SimpleSubscriber<ChildMessage> subscriber = new SimpleSubscriber<ChildMessage>();
            broker.Subscribe<ChildMessage>(subscriber);
            ChildMessage msg = new ChildMessage() { IntValue = 19, StringValue = "Pietro" };
            await broker.Publish(msg);
            await Task.Delay(250);
            Assert.That(subscriber.ReceivedMsgType, Is.EqualTo(typeof(ChildMessage).AssemblyQualifiedName));
        }
        
        [Test]
        public async Task SubscribeAndPublishOnCustomExchange() {
            SimpleSubscriber<ChildMessage> subscriber = new SimpleSubscriber<ChildMessage>(111);
            broker.Subscribe<ChildMessage>(subscriber);
            ChildMessage msg = new ChildMessage() { IntValue = 11, StringValue = "Cristina" };
            await broker.Publish(msg, CustomExchangeName);
            await Task.Delay(250);
            Assert.That(subscriber.ReceivedMsg, Is.EqualTo(msg));
        }
        
        [Test]
        public async Task PublishProvidingMessageTypeExplicitly() {
            SimpleSubscriber<ChildMessage> subscriber = new SimpleSubscriber<ChildMessage>();
            broker.Subscribe<ChildMessage>(subscriber);
            ParentMessage msg = new ChildMessage() { IntValue = 19, StringValue = "Pietro" };
            await broker.Publish(msg, typeof(ChildMessage));
            await Task.Delay(250);
            Assert.That(subscriber.ReceivedMsg, Is.EqualTo(msg));
            Assert.That(subscriber.ReceivedMsgType, Is.EqualTo(typeof(ChildMessage).AssemblyQualifiedName));
        }

        [Test]
        [Ignore("Work in progress...")]
        public async Task MessageHierarchy() {
            SimpleSubscriber<ParentMessage> parentSub = new SimpleSubscriber<ParentMessage>();
            SimpleSubscriber<ChildMessage> childSub = new SimpleSubscriber<ChildMessage>();
            broker.Subscribe<ParentMessage>(parentSub);
            broker.Subscribe<ChildMessage>(childSub);
            ParentMessage msg = new ChildMessage() { IntValue = 19, StringValue = "Pietro" };
            await broker.Publish(msg, typeof(ChildMessage));
            await Task.Delay(250);
            Assert.That(parentSub.ReceivedMsg, Is.EqualTo(msg));
            Assert.That(childSub.ReceivedMsg, Is.EqualTo(msg));
            Assert.That(parentSub.ReceivedMsgType, Is.EqualTo(typeof(ChildMessage).AssemblyQualifiedName));
            Assert.That(childSub.ReceivedMsgType, Is.EqualTo(typeof(ChildMessage).AssemblyQualifiedName));
        }
    }

    internal class SimpleSubscriber<TMessage> : Subscriber<TMessage> {
        internal string ReceivedMsgType { get; private set; }
        internal TMessage ReceivedMsg { get; private set; }
        internal int N { get; private set; }

        public SimpleSubscriber(int n = 1919) {
            N = n;
        }

        public Task Consume(string msgType, TMessage msg) {
            ReceivedMsg = msg;
            ReceivedMsgType = msgType;
            return Task.FromResult(0);
        }
    }
}
