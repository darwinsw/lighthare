﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;

namespace LightHare {
    public class MessageBroker {
        private readonly string queueName;
        private readonly ConnectionFactory connectionFactory;
        private IConnection connection;
        private IModel channel;


        public MessageBroker(string rabbitMqUri, string queueName) {
            this.queueName = queueName;
            connectionFactory = new ConnectionFactory
            {
                Uri = new Uri(rabbitMqUri),
                AutomaticRecoveryEnabled = true,
                TopologyRecoveryEnabled = true
            };
        }

        public Task Start() {
            return Task.Run(() => SyncStart());
        }

        private static Task EmptyHandler(string msgType, object msg) {
            return Task.FromResult(0);
        }

        private void SyncStart() {
            if (connection == null || !connection.IsOpen)
            {
                connection = connectionFactory.CreateConnection();
            }

            if (channel == null || channel.IsClosed)
            {
                channel = connection.CreateModel();
            }
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (ch, ea) =>
            {
                byte[] body = ea.Body;
                string receivedText = Encoding.UTF8.GetString(body);
                Type messageType = Type.GetType(ea.BasicProperties.Type);
                object receivedMessage = DeserializeObject(receivedText, messageType);
                Subscriber handler = messageType != null ? handlers[messageType] : null;
                
                if (handler != null) {
                    var consume = handler.GetType().GetMethod("Consume", new[] { typeof(string), messageType });
                    //var generic = consume.MakeGenericMethod(messageType);
                    Task task = (Task)consume.Invoke(handler, new[] { ea.BasicProperties.Type, receivedMessage });
                    await task.ConfigureAwait(false);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else {
                    channel.BasicNack(ea.DeliveryTag, false, true);
                }
            };
            string consumerTag = channel.BasicConsume(queueName, false, consumer);
        }
        
        public Task Stop() {
            return Task.Run(() => SyncStop());
        }

        private void SyncStop()
        {
            try
            {
                channel.Close();
                connection.Close();
            }
            finally
            {
                channel = null;
                connection = null;
            }
        }

        public void Dispose()
        {
            Stop();
        }

        private IDictionary<Type, Subscriber> handlers = new ConcurrentDictionary<Type, Subscriber>();

        public void Subscribe<TMessage>(Subscriber<TMessage> subscriber) {
            handlers.Add(typeof(TMessage), subscriber);
        }

        private static object DeserializeObject(string receivedText, Type messageType) {
            return JsonConvert.DeserializeObject(receivedText, messageType);
        }

        public Task Publish<TMessage>(TMessage msg, string exchangeName = null) {
            return Publish(msg, typeof(TMessage), exchangeName);
        }
        
        public Task Publish(object msg, Type messageType, string exchangeName = null) {
            return Task.Run(() => {
                string json = JsonConvert.SerializeObject(msg);
                byte[] data = Encoding.UTF8.GetBytes(json);
                channel.TxSelect();
                channel.BasicPublish(GetExchangeName(exchangeName, messageType), "", new BasicProperties {
                    Type = GetMessageType(messageType)
                }, data);
                channel.TxCommit();
            });
        }

        private string GetMessageType(Type type) {
            return type.AssemblyQualifiedName;
        }

        private string GetExchangeName(string exchangeName, Type type) {
            return exchangeName ?? type.AssemblyQualifiedName;
        }
    }

    public interface Subscriber<in TMessage> : Subscriber {
        Task Consume(string msgType, TMessage msg);
    }
    
    public interface Subscriber {
    }
}