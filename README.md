# LightHare #

## Summary

A lightweight wrapper library over standard RabbitMq APIs.

## Development environment

In order to run the integration tests suite in your local development environment, you need

- RabbitMq responding on `amqp://guest:guest@localhost/integration-tests`

If you don't like these defaults, you can change them providing RabbitMq connection Url as `INTEGRATION_RABBITMQ_URI` environment variables.

Moreover, this approch is adopted in CI/CD pipelines, providing a specific hostname for RabbitMq service, in order to match services configuration in [`.gitlab-ci.yml`](https://gitlab.com/darwinsw/lighthare/blob/master/.gitlab-ci.yml)


| Pipeline | Status |
| ------------- |:--------------|
| Master pipeline      | [![pipeline status](https://gitlab.com/darwinsw/lighthare/badges/master/pipeline.svg)](https://gitlab.com/darwinsw/lighthare/commits/master) |
| Release pipeline | [![pipeline status](https://gitlab.com/darwinsw/lighthare/badges/release/pipeline.svg)](https://gitlab.com/darwinsw/lighthare/commits/release) |
